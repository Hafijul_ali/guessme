import PySimpleGUI as gui

WINDOW_WIDTH = 600
NUMBER_LIST = [0, 10, 100, 1000, 10000, 100000, 1000000]

gui.theme("Material 2")

window_layout = [
    [gui.Text("Guess Me", size=(WINDOW_WIDTH, 1), font=(
        "Comic Sans", 20), justification="center")],
    [gui.Text("_"*40, size=(WINDOW_WIDTH, 2), justification="center")],
    [gui.Text("Think of a number, but don't tell us right away...")],
    [gui.Text("Enter the range of the number")],
    [gui.Text("From:"), gui.Combo(NUMBER_LIST, default_value=0, key="_minValue_"),
     gui.Text("To:"), gui.Combo(NUMBER_LIST, default_value=100, key="_maxValue_"), gui.Button("Ok", key="_Ok_")],
    [gui.Text("Our guess is,", key="_guessBoard_", size=(15,1), font=("Comic Sans", 15))],
    [gui.Text("_"*40, size=(WINDOW_WIDTH, 2), justification="center")],
    [gui.Text("Guessed number is:"), gui.Button("Too high",disabled=True), gui.Button("Too low",disabled=True), gui.Text("or"), gui.Button("Equal to",disabled=True), gui.Text("original number")]
]

window = gui.Window("GuessMe", window_layout, size=(WINDOW_WIDTH, 300))

def validate_inputs(min_value, max_value):
    if min_value > max_value:
        return max_value, min_value
    elif min_value == max_value:
        gui.popup('Error!!! Kindly enter valid range of number')
        return (None,None)
    return min_value, max_value

while True:
    event, values = window.read()

    if event in (gui.WIN_CLOSED, "Exit"):
        break
    if event == "_Ok_":
        min_value, max_value = validate_inputs(int(values["_minValue_"]), int(values["_maxValue_"]))
        if max_value:
            guessed_number = (min_value+max_value)//2
            window["_guessBoard_"].update("Our guess is, {0}".format(guessed_number))
            window["_Ok_"].update(disabled=True)
            window["Too high"].update(disabled=False)
            window["Too low"].update(disabled=False)
            window["Equal to"].update(disabled=False)
    if event == "Too high":
        max_value = guessed_number
    elif event == "Too low":
        min_value = guessed_number
    elif (max_value == min_value) or event == "Equal to":
        gui.popup("{0} is your number".format(guessed_number))
    guessed_number = (min_value + max_value) // 2
    window["_guessBoard_"].update("Our guess is, {0}".format(guessed_number))


